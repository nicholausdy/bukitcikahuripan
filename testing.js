var Users = require('./users.js');
var Token = require('./token.js');
const perf = require('execution-time')();
async function main() {
	perf.start();
	//test user.js
	let res2 = await Users.insertRecord('james','james');
	console.log(res2);
	let res1 = await Users.findRecord("james","james");
	console.log(res1);
	//test token.js
	var res3 = await Token.generateToken('nicholaus');
	req = {
		headers : {
			'x-access-token':'Bearer '+res3,
			'CORS':''
		}
	}
	var res4 = await Token.checkToken(req)
	console.log(res3);
	console.log(res4);
	const execution = perf.stop();
	console.log('Execution time: '+execution.time);
}
main();