const bcrypt = require('bcrypt');
const saltRounds = 10; //cost of processing data -> more equals more secure
const { Pool, Client } = require('pg');
require('dotenv').config();
const errorHandler = require('./error_handler.js')

//insert parameter from environment variable to construct pool object
const pool = new Pool({
	user: process.env.DB_USER,
	host: process.env.DB_HOST,
	database: process.env.DB_NAME,
	password: process.env.DB_PASS,
	port: process.env.DB_PORT
});

//hash password using bcrypt
async function hashPassword(password) {
	const hashedPassword = await new Promise((resolve,reject)=>{
		bcrypt.hash(password, saltRounds, function(err,hash){
			if (err) { 
				reject (err)
			}
			else {
				resolve (hash)
			}

		});
	})
	return hashedPassword
}

//compare hashed password to plain text password
async function comparePassword(password,hash) {
	const result = await new Promise((resolve,reject)=>{
		bcrypt.compare(password,hash, function(err,success){
			if (err) {
				reject (err)
			}
			else {
				resolve (success)
			}
		});
	})
	return result
}
async function findRecord(username,password) {
	try {
		//generate sql statement
		const sql = 'SELECT * FROM users WHERE username = $1';
		const values = [username];
		//do query
		const result = await pool.query(sql,values);
		//check whether username exists
		if (result.rowCount == 0) {
			return {Message: 'Username not found'}
		}
		else {
			const compareResult = await comparePassword(password,result.rows[0].password)
			//check whether password is correct using bcrypt
			if (compareResult){
				return 	{
							Message: 'Login authorized',
							user_role: result.rows[0].user_role
						}
			}
			else {
				return {Message: 'Wrong password'}
			}
		}
	} 
	catch (e){
		return await errorHandler.databaseError(e);
	}	
}

async function insertRecord(username,password,user_role) {
	try {
		//generate sql statement
		const sql = 'INSERT INTO users(username,password,user_role) VALUES ($1,$2,$3)';
		const hashedPassword = await hashPassword(password);
		const values = [username, hashedPassword,user_role];
		//do query
		const result = await pool.query(sql,values);
		return {Message: 'Record insert successful'}
	}
	catch (e) {
		return await errorHandler.databaseError(e);	
	}
}

//export function
module.exports = {
	findRecord:findRecord,
	insertRecord:insertRecord
}


