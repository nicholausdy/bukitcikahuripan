//dependencies
const https = require('https')
const fs = require('fs') //filesystem read
const express = require('express'),
	bodyParser = require('body-parser');
const app = express()
const cors = require('cors');
const Users = require('./users.js');
const Token = require('./token.js');
//security hardening
const helmet = require('helmet')
//performance measurement
const perf = require('execution-time')();

//app setting
app.use(cors());
app.use(bodyParser.json({limit:'200mb'}));
app.use(bodyParser.urlencoded({
	limit:'200mb',
	extended: true
}));
app.use(helmet())

//routes and HTTP verbs
app.post('/login', async (req,res) => {
	try {
		perf.start()
		let userCheck = await Users.findRecord(req.body.username,req.body.password)
		if (userCheck.Message == 'Login authorized'){
			let access_token = await Token.generateToken(req.body.username,userCheck.user_role)
			res.status(200)
			res.json({"Message": "Login authorized","Token":access_token,"Username":req.body.username})
		}
		else if ((userCheck.Message == 'Username not found') || (userCheck.Message == 'Wrong password')){
			res.status(401)
			res.json(userCheck)
		}
		else if (userCheck.Message == 'Database connection error'){
			res.status(504)
			res.json(userCheck)
		}
		else {
			res.status(500)
			res.json(userCheck)
		}
	}
	catch (error){
		res.status(500)
		res.json({"Message":"Internal server error"})
	}
	finally {
		let execution = perf.stop()
		console.log('Execution time: '+execution.time);
	}
});

app.post('/register', async (req,res) => {
	try {
		perf.start()
		//validate token
		let tokenCheck = await Token.checkToken(req)
		if (tokenCheck.success){
			//check whether user is authorized or not
			if (tokenCheck.user_role == "admin"){
			//insert record
				let result = await Users.insertRecord(req.body.username,req.body.password,req.body.user_role)
				if (result.Message == 'Record insert successful'){
					res.status(200)
					res.json(result)
				}
				else if (result.Message == 'Database connection error'){
					res.status(504)
					res.json(result)
				}
				else if (result.Message == 'Primary key already exists'){
					res.status(400)
					res.json(result)
				}
				else {
					res.status(500)
					res.json(result)
				}
			}
			else {
				res.status(403)
				res.json({"Message":"User not authorized"})
			}
		}
		else {
			res.status(401)
			res.json(tokenCheck)
		}
	}
	catch (error) {
		res.status(500)
		res.json({"Message":"Internal server error"})
	}
	finally {
		let execution = perf.stop()
		console.log('Execution time: '+execution.time);
	}
});

//start app
https.createServer({
	key: fs.readFileSync('server.key'),
	cert: fs.readFileSync('server.cert')
}, app).listen(3001, () => {
	console.log('Maid cafe serving at port 3001')
    console.log('Ctrl+C to Terminate Process')
});