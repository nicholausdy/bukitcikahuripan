async function databaseError(e) {
	//duplicate primary key error handler
	if (e.code == '23505') {
		return {Message: 'Primary key already exists'}
	}
	//connection refused
	else if (e.code == 'ECONNREFUSED'){
		return {Message: 'Database connection error'}
	}
	else {
		return {Message: 'Unknown error encountered', Detail: e}
	}	
}

async function tokenError(e) {
	if (e.name == 'JsonWebTokenError'){
		return {
			success: false,
			message: 'Token invalid'
		}
	}
	else if (e.name == 'TokenExpiredError'){
		return {
			success: false,
			message: 'Token expired'
		}
	}
	else if (e instanceof TypeError){
		return {
			success: false,
			message: 'Invalid header'
		}
	}
	else {
		return {
			success: false,
			message: 'Unknown error',
			detail: e
		}
	}
}
//export function
module.exports = {
	databaseError:databaseError,
	tokenError:tokenError
}
