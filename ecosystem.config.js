module.exports = {
  apps : [{
    name: "api_bukitcikahuripan",
    script: "./server.js", //entry point
    instances: "max", //max number of app instances based on CPU number
    exec_mode: "cluster", //enable clustering and load balancing
    out_file: "./out.log", //create output log
    error_file: "./err.log", //create error log
    log_file: "./combined.log", //create combined output + error log
    //customize when in production
    max_memory_restart: "250M" //if memory usage >250M, auto restart program to clear RAM
  }]

  //deploy : {
  //  production : {
  //    user : 'node',
  //    host : '212.83.163.1',
  //    ref  : 'origin/master',
  //    repo : 'git@github.com:repo.git',
  //    path : '/var/www/production',
  //    'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
  //  }
  //}
};
