const jwt = require('jsonwebtoken');
const errorHandler = require('./error_handler.js')
require('dotenv').config();

//check token in authorization header
async function checkToken(req) {
	try {
		var token = await req.headers['x-access-token'] || req.headers['authorization']; // get request header
		if (token.startsWith('Bearer ')){
			token = token.slice(7, token.length); //Remove 'Bearer' by getting sub-string from 7th character to the end of character
		}
		//check whether token exists in appropriate header
		if (token) {
			// verify token based on TOKEN_SECRET env variable and expiration date
			jwt.verify(token, process.env.TOKEN_SECRET, function(err,decoded){
				if (err) {
					result = errorHandler.tokenError(err)
				}
				else {
					result = {
						success: true,
						message: 'Token accepted',
						username: decoded.username,
						user_role: decoded.user_role
					}
				}
			});
		}
		else {
			result = {
				success: false,
				message: 'Auth token is not provided'
			};
		}
		return result;
	}
	catch (e) {
		return await errorHandler.tokenError(e);
	}
}

//generate token to store in client-side
async function generateToken(username,user_role){
	const result = await new Promise((resolve,reject)=>{
		jwt.sign({username:username,user_role:user_role}, process.env.TOKEN_SECRET, {expiresIn: '24h'}, function(err,token){
			if (err){
				reject (err)
			}
			else {
				resolve(token)
			}
		});
	})
	return result
}

module.exports = {
	checkToken:checkToken,
	generateToken:generateToken
}